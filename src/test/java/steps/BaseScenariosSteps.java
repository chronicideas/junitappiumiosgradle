package steps;

import appium.CreateDriver;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.AndyWarhol;
import pages.BasePage;

public class BaseScenariosSteps {

    private BasePage basePage = new BasePage(CreateDriver.getInstance().getDriver());
    private AndyWarhol andyWarhol = new AndyWarhol(CreateDriver.getInstance().getDriver());

    @Given("^the Artistry app has been launched$")
    public void the_Artistry_app_has_been_launched() {
        basePage.appFullyLaunched();
    }

    @Then("^I see \"([^\"]*)\" in the PageSource$")
    public void i_see_in_the_PageSource(String expectedPageSource) {
        basePage.validatePageSource(expectedPageSource);
    }

    @When("^I choose to view paintings by Andy Warhol$")
    public void iChooseToViewPaintingsByAndyWarhol() {
        basePage.viewPaintingsByAndyWarhol();
    }

    @And("^I decide to read more info on the Eight Elvises painting$")
    public void iDecideToReadMoreInfoOnTheEightElvisesPainting() {
        andyWarhol.viewMoreInfoOnEightElvises();
    }
}
