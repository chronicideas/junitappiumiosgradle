package pages;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.*;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static utils.extensions.XCTestExtensions.*;

public class BasePage {

    IOSDriver driver;

    public BasePage(IOSDriver driver) {
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    private String getPageSource() { return driver.getPageSource(); }

    @iOSXCUITFindBy(accessibility = "Pablo Picasso")
    private IOSElement btnPabloPicasso;

    @iOSXCUITFindBy(accessibility = "Andy Warhol")
    private IOSElement btnAndyWarhol;

    @iOSXCUITFindBy(className = "XCUIElementTypeTable")
    private IOSElement artistsTable;

    public void appFullyLaunched() {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOf(btnPabloPicasso));
    }

    public void validatePageSource(String expectedPageSource) {
        Assertions.assertTrue(getPageSource().contains(expectedPageSource));
    }

    public AndyWarhol viewPaintingsByAndyWarhol() {
//        xcScroll("down", "predicateString", "(type == 'XCUIElementTypeCell')[5]"); this works
//        xcScroll("down", "name", "Andy Warhol"); this works

        xcSwipe("up", 2); // this also works and is the fastest
        btnAndyWarhol.click();
        return new AndyWarhol(driver);
    }
}
