package appium;

import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class CreateDriver {

    private CreateDriver() {

    }

    private static CreateDriver instance = null;
    private static ThreadLocal<IOSDriver<IOSElement>> iosDriver = new ThreadLocal<>();

    private File file = new File("src");
    private File app = new File(file + "/Artistry.app");
    private DesiredCapabilities cap = new DesiredCapabilities();

    public static CreateDriver getInstance() {
        if (instance == null) {
            instance = new CreateDriver();
        }
        return instance;
    }

    //Simulators
    public void setDriverOnSim(String platformVersion, String deviceName) throws MalformedURLException {
        AppiumServer.start(4723);

        cap.setCapability("automationName", "XCUITest");
        cap.setCapability("platformVersion", platformVersion);
        cap.setCapability("deviceName", deviceName);
        cap.setCapability("fullReset", true);
        cap.setCapability("app", app.getAbsolutePath());

        setDriverSession(cap);
    }

    //Real Connected Devices
    public void setDriverOnRealDevice(String udid, String platformVersion, String deviceName) throws MalformedURLException {
        cap.setCapability("xcodeOrdId", "AG96JLZM8H");
        cap.setCapability("xcodeSigningId", "iPhone Developer");
        cap.setCapability("udid", udid);

        setDriverOnSim(platformVersion, deviceName);
    }

    private void setDriverSession(DesiredCapabilities cap) throws MalformedURLException {
        iosDriver.set(new IOSDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap));
    }

    public IOSDriver<IOSElement> getDriver() {
        return iosDriver.get();
    }

    public synchronized void tearDown() {
        iosDriver.get().quit();
        AppiumServer.stop();
    }
}
