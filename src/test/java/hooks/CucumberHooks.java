package hooks;

import appium.CreateDriver;
import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.net.MalformedURLException;

public class CucumberHooks {

    @Before("@SimIphone8")
    public void beforeIphone8() throws MalformedURLException {
        CreateDriver.getInstance().setDriverOnSim("12.1", "iPhone 8");
    }

    @Before("@SimIphoneX")
    public void beforeIphoneX() throws MalformedURLException {
        CreateDriver.getInstance().setDriverOnSim("12.1", "iPhone X");
    }

    @Before("@RealIphone6s")
    public  void beforeIphone6s() throws MalformedURLException {
        CreateDriver.getInstance().setDriverOnRealDevice("2c3207b85c6df6f08b171e5ee61d96406abb4549", "12.1.2", "iPhone");
    }

    @After
    public void after() {
        CreateDriver.getInstance().tearDown();
    }
}
